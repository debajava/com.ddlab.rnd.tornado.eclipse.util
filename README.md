# README #

This is a small eclipse plugin developed by Debadatta Mishra to to facilitate the java developers to speed up their work.

### A Light Weight Eclipse Utility Plugin ###

# Feature List #
* Copy File Path (Windows Style , Multi Selection)
* Copy File Path (Unix Style , Multi Selection)
* Copy Files and Folders (Multi Selection)
* Copy internal java library
* Copy Maven library
* Open in Windows Command Prompt (Multi Selection)
* Open Command Prompt in Eclipse Console
* Open in Windows Explorer (Multi Selection)
* Full Screen
* Shortcut for Windows Command Prompt from Eclipse console
* Send Files to Desktop
* Zip files and send to Desktop
* Capture screenshot and save to Desktop
* Split Eclipse Text Editor
* System view and Kill a Process Id 
* Preferences for Utilities 

### How do I get set up? ###

* Import the project using any git utility and setup in your eclipse workspace.
* Download Eclipse Ganemede and point to ant build file.
* Run the ant build file from Eclipse
* Go to the dist folder in the project and install the zip file in eclipse.


### Who do I talk to? ###

* Debadatta Mishra
* debadatta.mishra@gmail.com